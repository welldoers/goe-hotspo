# How to use
To use this project you have to copy the `public` folder into the appropriate folder on the Mikrotik router.

## Editing the files
The source files are in `src` folder but they are not usable in their raw form.
You have to compile them. To do so, you need `Node.js`.

If you don't have Node, install it. Then just run 'npm run dev' to be able to see a browser with a list of elements.
To compile the files and prepare them for the public, just type `npm run build` and it will build the files for you.

## Legacy
The legacy folder contains the old, edited filed from the GoE Admin.
