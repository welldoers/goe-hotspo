import Vue from 'vue'
import Vuelidate from 'vuelidate'
import Login from './components/login.vue'

Vue.use(Vuelidate)

new Vue({
  el: '#app',
  components: {
    Login
  }
})