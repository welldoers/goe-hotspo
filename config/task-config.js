//var UglifyJSPlugin = require('uglifyjs-webpack-plugin')
module.exports = {
  html: true,
  images: true,
  fonts: true,
  static: true,
  svgSprite: false,
  ghPages: false,
  stylesheets: true,

  javascripts: {
    babel: {
      'presets': [
        ['env', {'modules': false}],
        'stage-2'
      ],
      'plugins': ['transform-runtime'],
      'comments': false
    },
    entry: {
      // files paths are relative to
      // javascripts.dest in path-config.json
      app: ['./app.js']
    },
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      }
    ]
//    production: {
//      plugins: (webpack) => {
//        return [
//          new UglifyJSPlugin()
//        ]
//      }
//    }
  },
  browserSync: {
    server: {
      // should match `dest` in
      // path-config.json
      baseDir: 'public'
    }
  },

  production: {
    rev: false
  }
}

